# test_bubble_sort.py

import unittest
from bubble_sort import bubble_sort

class TestBubbleSort(unittest.TestCase):

    def test_bubble_sort_empty_list(self):
        self.assertEqual(bubble_sort([]), [])

    def test_bubble_sort_with_one_element(self):
        self.assertEqual(bubble_sort([5]), [5])

    def test_bubble_sort_already_sorted_list(self):
        numbers = [1, 2, 3, 4, 5]
        self.assertEqual(bubble_sort(numbers), numbers)

    def test_bubble_sort_reverse_sorted_list(self):
        numbers = [5, 4, 3, 2, 1]
        expected_sorted_numbers = [1, 2, 3, 4, 5]
        self.assertEqual(bubble_sort(numbers), expected_sorted_numbers)

    def test_bubble_sort_random_list(self):
        numbers = [9, 3, 5, 1, 8, 7, 2, 6, 4]
        expected_sorted_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.assertEqual(bubble_sort(numbers), expected_sorted_numbers)

if __name__ == '__main__':
    unittest.main()
